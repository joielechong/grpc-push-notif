BUILD_FLAGS = -ldflags="-s -w" 
CLIENT_BINARY = bin/client
SERVER_BINARY = bin/server
build:
	#build client ...
	env GOOS=linux CGO_ENABLED=0 go build -a -installsuffix nocgo $(BUILD_FLAGS) -o $(CLIENT_BINARY) ./client/.
	#build server
	env GOOS=linux CGO_ENABLED=0 go build -a -installsuffix nocgo $(BUILD_FLAGS) -o $(SERVER_BINARY) ./server/.

dependencies:
	#installing dependencies ...
	go mod tidy

protoc:
	#generate protocol buffer(.proto) files ...
	@protoc --proto_path=protos protos/*.proto --go_out=plugins=grpc:pb

start-client:
	#starting client ...
	./bin/client

start-server:
	#starting server ...
	./bin/server 
