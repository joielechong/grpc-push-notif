# grpc-push-notif
forked from [grpc-push-notif](https://github.com/itisbsg/grpc-push-notif)

# grpc-push-notif
There are primarily 4 types of gRPC apis, req/resp, req/streaming-resp, streaming-req/streaming-resp and all gRPC apis are initiated by the client and gRPC, as of now, doesnt natively support server-initiated rpc calls. However, there are many use cases where a server may have to initiate a request. gRPC allows this mechanism in a bit of a twisted way. I have come across many questions on different forums requesting the same. The effort here is to really showcase that capability of grpc, it is done by having a long-lived streaming api.

The implementation here emulates a very simple, rudimentary smart thermostate which connects to its server, subscribes to a bunch notifications and listens to them. The screenshot below provides the overview, fairly simple.

## How to run project
1. First thing first, init the mod with:
   `go mod init grpcpushnotif`
2. run `make run dependencies` to install dependecies(go1.11+ required)
3. run `make build`
4. run `make start-server` to start the server
5. run `make start-client` for each client

 ## Demo:
push notif to specific client

### How to push notification to specific client:
Send by typing text with formatting like client_number:message in server app terminal. For example:
   `3:hello`

You can get the `client_number` in client terminal.   

![Screenshot](screenshot.gif)
